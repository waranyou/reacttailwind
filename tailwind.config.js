/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        'navbar-color'  : '#000E1E',
        'table-color' :'#667085',
        'icon-color' : '#000E1E'
      },
    },
  },
  plugins: [],
}

