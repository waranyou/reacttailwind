import { LanguageContext } from "../contexts/language";
import React, { useContext } from "react";
//import "./Home.css"
export default function Home() {
  const { t } = useContext(LanguageContext);
  return (
    <>
                  {/* align-item    font-size   height */}
      <p className=" text-center text-[3rem] leading-[90vh]">{t("Home:title")}</p>
    </>
  );
}
