import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { LanguageContext } from "../contexts/language";
import "./Branch.css";
import { AiOutlineSearch } from "react-icons/ai";
import { AiOutlinePlus } from "react-icons/ai";
import ReactPaginate from 'react-paginate';

export default function Branches() {
  const { t } = useContext(LanguageContext);
  const [data, setData] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);

  const itemsPerPage = 10;
  const pagesVisited = pageNumber * itemsPerPage;

  const displayData = data.slice(pagesVisited, pagesVisited + itemsPerPage).map(item => (
    <tr key={item.id} className="border-b-4 border-black-500">
      <td>{item.id}</td>
      <td>{item.name}</td>
              <td>{item.phoneNumber}</td>
              <td>
                <img
                  className="mx-auto"
                  alt={item.branchImageUrl}
                  src={item.branchImageUrl}
                ></img>
              </td>
    </tr>
  ));

  const pageCount = Math.ceil(data.length / itemsPerPage);

  const handlePageClick = ({ selected }) => {
    setPageNumber(selected);
  };

  useEffect(() => {
    axios
      .get("https://cc-cached.devsmith4289.workers.dev/api/v1/public/branches")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);



  return (
    <>
      <p className="ml-[150px] mt-[90px] mb-[20px] text-[25px] ">
        สาขาทั้งหมด({data.length})
      </p>
      <div className="justify-between flex">
        <label class="relative block  ml-[150px]">
          <span class="sr-only">Search</span>
          <span class="absolute inset-y-0 left-0 flex items-center pl-2">
            <AiOutlineSearch />
          </span>
          <input
            class=" placeholder:text-slate-400 block bg-white  border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
            placeholder="  ค้นหาสาขา"
            type="text"
            name="search"
          />
        </label>

        <label class="relative block mr-[150px] ">
          <span class="sr-only">Search</span>
          <span class="absolute inset-y-0 left-0 flex items-center pl-2 text-white">
            <AiOutlinePlus />
          </span>

          <button
            class="ml-[10px] text-White block bg-black  border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
            onclick="myFunction()"
          >
            เพิ่มสาขา
          </button>
        </label>
      </div>
      <table className="w-5/6 mt-[50px] mb-[100px] shadow-lg shadow-black">
        <thead className="bg-table-color  ">
          <tr>
            <th className="rounded-tl-[1rem]">{t("Branch:id")}</th>
            <th>{t("Branch:name")}</th>
            <th>{t("Branch:Phonenumber")}</th>
            <th className="rounded-tr-[1rem]">{t("Branch:Photo")}</th>
          </tr>
        </thead>
        <tbody>
        {displayData}
        </tbody>
      </table>
      <ReactPaginate 
        pageCount={pageCount}
        onPageChange={handlePageClick}
        
        containerClassName={'flex items-center justify-center mt-8 mb-4'}
        
        activeClassName={'active'}
        
        pageClassName={'page-item border-2 border-black-600 rounded-[20px] mr-[2px]'}
        pageLinkClassName={'page-link'}
        
        previousClassName={'page-item border-2 border-black-600  mr-[2px] rounded-[20px] mr-[2px]'}
        previousLinkClassName={'page-link '}
        
        nextClassName={'page-item border-2 border-black-600  rounded-[20px] mr-[2px]'}
        nextLinkClassName={'page-link '}
      />
    </>
  );
}
