import React from "react";
import * as AiIcons from "react-icons/ai";
 import * as IoIcons from "react-icons/io";
//  import { useContext } from "react";
//  import { LanguageContext } from "../contexts/language";
export const SidebarData = [

    {
         title: 'Home',
         path: '/',
          icon: <AiIcons.AiFillHome />,
          cName: 'nav-text'
      },
     {
          title: 'Branch',
          path: '/Branches',
          icon: <IoIcons.IoIosPaper />,
          cName: 'nav-text'
     }

  ]


