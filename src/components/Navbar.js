import { Link } from "react-router-dom";
import "./Navbar.css";

import { LanguageContext } from "../contexts/language";
import React, { useContext, useState } from "react";

import { IconContext } from "react-icons";
import * as FaIcons from "react-icons/fa";
import { SidebarData } from "./SidebarData";

import avaterUser from "../avatar.png";
import logoCC from "../logo_codeclean.png";

export default function Navbar() {
  const [sidebar, setSidebar] = useState(true);
  const showSidebar = () => setSidebar(!sidebar);
  const { setLanguage, t } = useContext(LanguageContext);
  return (
    <>
      <nav className="items-center bg-navbar-color fixed top-0 w-full z-30">
        <IconContext.Provider value={{}}>
          <div className="navbar text-[#f5f5f5]">
            <Link to="#" className="menu-bars">
              <FaIcons.FaBars onClick={showSidebar} />
            </Link>
          </div>
          <nav
            className={
              sidebar
                ? "nav-menu active w-[85px] z-10 fixed"
                : "nav-menu w-[85px] z-10 fixed"
            }
          >
            <ul className="nav-menu-items shadow-lg shadow-black bg-white">
              <li className="navbar-toggle">
                <Link to="#" className="menu-bars bg-icon-color">
                  {/* <AiFaIcons.AiFillCloseCircle /> */}
                  <p className="text-[15px] mt-[10px] ml-[-7px]">
                    {t("Sidebar:Menu")}
                  </p>
                </Link>
              </li>
              {/*ตัวแปร,ลำดับ */}
              {SidebarData.map((item, index) => {
                return (
                  <li key={index} className={item.cName}>
                    <Link to={item.path}>
                      {item.icon}
                      {/* <span >
                            {item.title}    
                         {t('Sidebar:Number')[index]} 
                    </span> */}
                    </Link>
                  </li>
                );
              })}
            </ul>
          </nav>
        </IconContext.Provider>
        <div>
          <img
            src={logoCC}
            alt="my logo1"
            className="w-20 h-20 border-none p-4 shadow-none p-4"
          />
        </div>

        <Link to="/" className="items-center mr-auto text-[#f5f5f5]">
          <h3>{t("Home:logo")}</h3>
        </Link>
        <div>
          <img
            src={avaterUser}
            alt="My Image1"
            className="w-20 h-20 border-none p-4 shadow-none p-4"
          />
        </div>
        <div className="ml-[2px] mr-[15px]">
          <p className="text-[#f5f5f5]">Title Name(Subtitle)</p>
          <p className="text-[#f5f5f5] ">Detail 1 Detail 2 Detail3</p>
          <button onClick={() => setLanguage("th")}>ไทย /</button>
          <button onClick={() => setLanguage("en")}>Eng</button>
        </div>
      </nav>
    </>
  );
}
