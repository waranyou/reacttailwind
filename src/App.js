// import logo from './logo.svg';
import Home from './Page/Home';
import Branches from './Page/Branch'
import Navbar from './components/Navbar';
import { BrowserRouter,Routes,Route } from 'react-router-dom';
//import Sidebar from './components/sildebar';
import './App.css';

function App() {
  return (
    <BrowserRouter>
    <Navbar/>
      <Routes>
        <Route path="/" element={<Home/>}></Route>
        <Route path="/Branches" element={<Branches/>}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
