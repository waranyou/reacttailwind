import { createContext, useEffect, useState } from "react";

export const LanguageContext = createContext({});

export const LanguageProvider = ({ children }) => {
  const [language, setLanguage] = useState("th");
  const [useLanguage, setUseLanguage] = useState({});

  useEffect(() => {
    async function fetchData() {
      const lang = await import(`../language/${language}.json`);
      setUseLanguage(lang);
    }
    fetchData();
  }, [language]);

  let t = (key) => {
    return useLanguage[key] || key;
  };

  return (
    <LanguageContext.Provider
      value={{
        language,
        setLanguage,
        t,
        
      }}
    >
      {children}
    </LanguageContext.Provider>
  );
};
